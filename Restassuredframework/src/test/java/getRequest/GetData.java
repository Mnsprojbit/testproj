package getRequest;

import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import io.restassured.response.Response;


public class GetData {
	

	@Test(priority=1) 
	public void testresponsecode() {
		
		Response resp= get("http://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22");
		
		int data=resp.getStatusCode();
		System.out.println("Passed status code is" +data);
		Assert.assertEquals(200, data);
		//Directly we can get the status code
		int data1=get("http://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22").getStatusCode();
		System.out.println("Passed status code is" +data1);
	}
	
	@Test(priority=2)
	public void testresponsecode1() {
		
		Response resp= get("http://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22");
		
		int code=resp.getStatusCode();
		System.out.println("Failure status code is" +code);
		Assert.assertEquals(400, code);
	}
	
	@Test(priority=3)
	public void testbody() {
		
		Response resp= get("http://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22");
		
		String code=resp.asString();
		System.out.println("status code is" +code);
		
		//Directly we can get the data
		String code1=get("http://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22").asString();
		System.out.println("status code is" +code1);
	}
	
	@Test(priority=4)
	public void testcontenttype() {
		
		Response resp= get("http://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22");
		
		String code=resp.contentType();
		long rtime=get("http://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22").getTime();
		String header=resp.getHeader("content-length");
		String cookie=resp.getCookie("cookieName");
		System.out.println("content type is" +code);
		System.out.println("respone time is" +rtime);
		System.out.println("header is" +header);
		System.out.println("cookie is" +cookie);
		
	}
}
