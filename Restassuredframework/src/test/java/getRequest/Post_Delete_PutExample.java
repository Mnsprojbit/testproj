package getRequest;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
//import io.restassured.RestAssured.*;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Post_Delete_PutExample {
	
	@Test
	public void test1() {
		
		RequestSpecification request=RestAssured.given();
		//Header part same way in postman we pass key and value
		request.header("Content-Type","application/json");
		//body part similar way like postman
		JSONObject json=new JSONObject();
		json.put("id", "10");
		json.put("title", "selenium");
		json.put("author", "test");
		json.put("createddate", "today");
		request.body(json.toJSONString());
		//Selected method type post and passed url value 
		Response response=request.post("http://localhost:3000/posts/");
		
		//status code
		int code=response.getStatusCode();
		System.out.println(code);
		
		//compares actual status code with expected status code 
		Assert.assertEquals(code, 201);
	}
	

}
