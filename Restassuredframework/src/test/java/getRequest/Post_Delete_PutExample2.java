package getRequest;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
//import io.restassured.RestAssured.*;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class Post_Delete_PutExample2 {
	
	@Test
	public void test1() {
		
		
		RequestSpecification request=RestAssured.given();
		
		Response response=request.delete("http://localhost:3000/posts/10");
		
		int code=response.getStatusCode();
		System.out.println(code);
		Assert.assertEquals(code, 200);
	}
	

}
